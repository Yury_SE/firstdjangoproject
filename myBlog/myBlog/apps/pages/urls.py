from django.urls import path
from . import views

urlpatterns = [
    # path('', views.homePageView, name='home'),
    path('', views.HomeViewPage.as_view(), name='home'),
    # path('home/', views.HomeViewPage.as_view(), name='home'),
    path('about/', views.AboutViewPage.as_view(), name='about'),
]
