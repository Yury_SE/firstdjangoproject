from django.http import HttpResponse
from django.views.generic import TemplateView


# def homePageView(request):
#     return HttpResponse("Yeah!")


class HomeViewPage(TemplateView):
    template_name = "home.html"


class AboutViewPage(TemplateView):
    template_name = "about.html"
